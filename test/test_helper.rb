$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'json_schema_cli_tools'
require 'minitest/autorun'

module TestUtil

  def temp_file(contents='')
    file = Tempfile.new("temp")
    file.write(contents)
    file.size()
    yield(file.path)
  ensure
    file.close
    file.unlink
  end

end
