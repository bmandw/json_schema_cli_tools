require "test_helper"
require "json_schema"
require "json_schema_cli_tools/query_params_conversion"
require "json_schema_cli_tools/util"

class TypeConversionTest < Minitest::Test

  def setup
    link_schema = schema.definitions["user"].links[0].schema
    @converter = JsonSchemaCliTools::QueryParamsConversion.new(link_schema)
  end

  def test_convert
    data = query_params("date_from=2016-12-31&detail=true&min=123.45&age=25&group[]=1&group[]=2")
    result = @converter.convert(data)
    assert_equal result, {
      "date_from" => "2016-12-31",
      "detail" => true,
      "min" => 123.45,
      "age" => 25,
      "group" => [1, 2]
    }
  end

  def test_convert_when_passed_value_is_not_boolean_to_the_property_which_boolean_is_defined
    data = query_params("detail=truty")
    e = assert_raises JsonSchema::AggregateError do
      @converter.convert(data)
    end
    assert_equal e.to_s, "#: failed schema #/definitions/user/links/0/schema/properties/detail: boolean must be true or false"
  end

  def test_convert_when_passed_value_is_not_number_to_the_property_which_number_is_defined
    data = query_params("min=abc")
    e = assert_raises JsonSchema::AggregateError do
      @converter.convert(data)
    end
    assert_equal e.to_s, "#: failed schema #/definitions/user/links/0/schema/properties/min: invalid value for Float(): \"abc\""
  end

  def test_convert_when_passed_value_is_not_integer_to_the_property_which_integer_is_defined
    data = query_params("age=abc")
    e = assert_raises JsonSchema::AggregateError do
      @converter.convert(data)
    end
    assert_equal e.to_s, "#: failed schema #/definitions/user/links/0/schema/properties/age: invalid value for Integer(): \"abc\""
  end

  def test_convert_when_passed_empty_value_to_the_property_which_null_is_defined
    data = query_params("date_from=2016-12-31&date_to=&detail=true")
    result = @converter.convert(data)
    assert_equal result, {
      "date_from" => "2016-12-31",
      "date_to" => nil,
      "detail" => true
    }
  end

  def test_convert_when_passed_empty_value_to_the_property_which_null_is_not_defined
    data = query_params("date_from=&date_to=2016-12-31&detail=true")
    result = @converter.convert(data)
    assert_equal result, {
      "date_from" => "",
      "date_to" => "2016-12-31",
      "detail" => true
    }
  end

  def test_convert_when_passed_value_is_not_array_to_the_property_which_array_is_defined
    data = query_params("group=1")
    result = @converter.convert(data)
    assert_equal result, {
      "group" => [1]
    }
  end

  def query_params(query_string)
    JsonSchemaCliTools::Util.query_string_to_hash(query_string)
  end

  def schema
    JsonSchema.parse!(schema_sample)
  end

  def schema_sample
    {
      "$schema" => "http://json-schema.org/draft-04/hyper-schema",
      "type" => [
        "object"
      ],
      "definitions" => {
        "user" => {
          "$schema" => "http://json-schema.org/draft-04/hyper-schema",
          "title" => "User",
          "description" => "system user.",
          "stability" => "prototype",
          "strictProperties" => true,
          "type" => [
            "object"
          ],
          "definitions" => {
            "id" => {
              "description" => "unique identifier of user",
              "readOnly" => true,
              "example" => 12345,
              "type" => [
                "number"
              ]
            },
            "name" => {
              "description" => "name of user",
              "example" => "test",
              "type" => [
                "string"
              ]
            }
          },
          "links" => [
            {
              "description" => "List existing users.",
              "href" => "/users",
              "method" => "GET",
              "schema" => {
                "properties" => {
                  "date_from" => {
                    "description" => "when user was created",
                    "format" => "date",
                    "example" => "2016-07-01",
                    "type" => [
                      "string"
                    ]
                  },
                  "date_to" => {
                    "description" => "when user was created",
                    "format" => "date",
                    "example" => "2016-07-01",
                    "type" => [
                      "null",
                      "string"
                    ]
                  },
                  "detail" => {
                    "description" => "detail",
                    "example" => "true",
                    "type" => [
                      "boolean"
                    ]
                  },
                  "min" => {
                    "description" => "min",
                    "example" => "123.45",
                    "type" => [
                      "number"
                    ]
                  },
                  "age" => {
                    "description" => "age",
                    "example" => "20",
                    "type" => [
                      "integer"
                    ]
                  },
                  "group" => {
                    "description" => "min",
                    "items" => {
                      "type" => [
                        "number"
                      ]
                    },
                    "type" => [
                      "array"
                    ]
                  }
                },
                "type" => [
                  "object"
                ]
              },
              "rel" => "instances",
              "title" => "List"
            }
          ],
          "properties" => {
            "id" => {
              "$ref" => "#/definitions/user/definitions/id"
            },
            "name" => {
              "$ref" => "#/definitions/user/definitions/name"
            }
          }
        }
      },
      "properties" => {
        "user" => {
          "$ref" => "#/definitions/user"
        }
      },
      "title" => "test api",
      "description" => "test web api",
      "links" => [
        {
          "href" => "http://localhost/api",
          "rel" => "self"
        }
      ],
      "id" => "test_api"
    }
  end

end
