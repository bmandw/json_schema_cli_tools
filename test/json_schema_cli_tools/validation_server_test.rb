require "test_helper"
require "tempfile"
require "tmpdir"
require "pathname"
require "rack/test"
require "logger"

class ValidationServerTest < Minitest::Test
  include Rack::Test::Methods

  def setup
    @response_data_dir_path = Pathname(Dir.mktmpdir)
    @logger = TestLogger.new(File::NULL)
  end

  def teardown
    FileUtils.remove_entry_secure(@response_data_dir_path)
  end

  def app
    local_schema = schema
    response_generator = JsonSchemaCliTools::Response.new(proxy_config)
    local_logger = @logger
    Rack::Builder.app do
      use JsonSchemaCliTools::ValidationServer, schema: local_schema, logger: local_logger, response_generator: response_generator
      run ->(env) { [404, {}, ["Not found"]] }
    end
  end

  def test_it_responds_dummy_data
    contents = '{"users":[]}'
    response_json("user", "List", contents) do |file_path|
      get "/users"

      assert_equal 200,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      log = @logger.last_logs[0]
      assert_equal Logger::INFO, log[:severity]
      assert_equal '[Handle Response API Server] "GET /users"', log[:progname]
    end
  end

  def test_it_responds_204_status_on_delete_request
    delete "/users/1"

    assert_equal 204,  last_response.status

    log = @logger.last_logs[0]
    assert_equal Logger::INFO, log[:severity]
    assert_equal '[Handle Response API Server] "DELETE /users/1"', log[:progname]
  end

  def test_it_validates_request_query_string_and_logging_error
    contents = '{"users":[]}'
    response_json("user", "List", contents) do |file_path|
      get "/users", { "date_from": "123" }

      assert_equal 200,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "GET /users?date_from=123"', logs[0][:progname]

      assert_equal Logger::ERROR, logs[1][:severity]
      assert_equal "[JSON Schema Invalid HTTP Request QueryString Error] GET /users =====>", logs[1][:progname]
      assert_equal Logger::ERROR, logs[2][:severity]
      assert_equal "  QueryString Params: {\"date_from\":\"123\"}", logs[2][:progname]
      assert_equal Logger::ERROR, logs[3][:severity]
      assert_equal "  Reason:", logs[3][:progname]
      assert_equal Logger::ERROR, logs[4][:severity]
      assert_equal "    #/date_from: failed schema #/definitions/user/links/1/schema/properties/date_from: 123 is not a valid date.", logs[4][:progname]
      assert_equal Logger::ERROR, logs[5][:severity]
      assert_equal "<=====", logs[5][:progname]
    end
  end

  def test_it_validates_request_query_string
    contents = '{"users":[]}'
    response_json("user", "List", contents) do |file_path|
      get "/users", { "date_from": "2015-12-31" }

      assert_equal 200,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "GET /users?date_from=2015-12-31"', logs[0][:progname]

      assert_equal Logger::INFO, logs[1][:severity]
      assert_equal '[HTTP Request Validation] OK', logs[1][:progname]

      assert_equal Logger::INFO, logs[2][:severity]
      assert_equal '[HTTP Response Validation] OK', logs[2][:progname]

      assert_equal 3, logs.length
    end
  end

  def test_it_validates_response_body_and_logging_error
    contents = '{}'
    response_json("user", "List", contents) do |file_path|
      get "/users"

      assert_equal 200,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "GET /users"', logs[0][:progname]

      assert_equal Logger::INFO, logs[1][:severity]
      assert_equal '[HTTP Request Validation] OK', logs[1][:progname]

      assert_equal Logger::ERROR, logs[2][:severity]
      assert_equal "[JSON Schema Invalid HTTP Response Error] GET /users =====>", logs[2][:progname]
      assert_equal Logger::ERROR, logs[3][:severity]
      assert_equal "  JSON File Path: #{file_path}", logs[3][:progname]
      assert_equal Logger::ERROR, logs[4][:severity]
      assert_equal "  Response Body: {}", logs[4][:progname]
      assert_equal Logger::ERROR, logs[5][:severity]
      assert_equal "  Reason:", logs[5][:progname]
      assert_equal Logger::ERROR, logs[6][:severity]
      assert_equal "    #: failed schema #/definitions/user/links/1/targetSchema: \"users\" wasn't supplied.", logs[6][:progname]
      assert_equal Logger::ERROR, logs[7][:severity]
      assert_equal "<=====", logs[7][:progname]
    end
  end

  def test_it_validates_request_body_data_and_logging_error
    contents = '{"id":12345,"name":"test"}'
    response_json("user", "Create", contents) do |file_path|

      post_json "/users", '{"name":1234}'

      assert_equal 201,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "POST /users"', logs[0][:progname]

      assert_equal Logger::ERROR, logs[1][:severity]
      assert_equal "[JSON Schema Invalid HTTP Request Error] POST /users =====>", logs[1][:progname]
      assert_equal Logger::ERROR, logs[2][:severity]
      assert_equal "  Request Body: {\"name\":1234}", logs[2][:progname]
      assert_equal Logger::ERROR, logs[3][:severity]
      assert_equal "  Reason:", logs[3][:progname]
      assert_equal Logger::ERROR, logs[4][:severity]
      assert_equal "    #/name: failed schema #/definitions/user/links/0/schema/properties/name: For 'properties/name', 1234 is not a string.", logs[4][:progname]
      assert_equal Logger::ERROR, logs[5][:severity]
      assert_equal "<=====", logs[5][:progname]
    end
  end

  def test_it_validates_request_body_data
    contents = '{"id":12345,"name":"test"}'
    response_json("user", "Create", contents) do |file_path|

      post_json "/users", '{"name": "test"}'

      assert_equal 201,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "POST /users"', logs[0][:progname]
      assert_equal Logger::INFO, logs[1][:severity]
      assert_equal '[HTTP Request Validation] OK', logs[1][:progname]
      assert_equal Logger::INFO, logs[2][:severity]
      assert_equal '[HTTP Response Validation] OK', logs[2][:progname]
      assert_equal 3, logs.length
    end
  end

  def test_it_validates_response_body_data_on_instances_rel
    contents = '[{"id":12345,"name":"test"}]'
    response_json("user", "List", contents) do |file_path|

      get "/extra_users"

      assert_equal 200,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "GET /extra_users"', logs[0][:progname]
      assert_equal Logger::INFO, logs[1][:severity]
      assert_equal '[HTTP Response Validation] OK', logs[1][:progname]
      assert_equal 2, logs.length
    end
  end

  def test_it_validates_request_body_data_and_logging_error_on_instances_rel
    contents = '[{"id":"12345","name":"test"}]'
    response_json("user", "List", contents) do |file_path|

      get "/extra_users"

      assert_equal 200,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "GET /extra_users"', logs[0][:progname]

      assert_equal Logger::ERROR, logs[1][:severity]
      assert_equal "[JSON Schema Invalid HTTP Response Error] GET /extra_users =====>", logs[1][:progname]
      assert_equal Logger::ERROR, logs[2][:severity]
      assert_equal "  JSON File Path: #{file_path}", logs[2][:progname]
      assert_equal Logger::ERROR, logs[3][:severity]
      assert_equal "  Response Body: [{\"id\":\"12345\",\"name\":\"test\"}]", logs[3][:progname]
      assert_equal Logger::ERROR, logs[4][:severity]
      assert_equal "  Reason:", logs[4][:progname]
      assert_equal Logger::ERROR, logs[5][:severity]
      assert_equal "    #/id: failed schema #/definitions/user/properties/id: For 'properties/id', \"12345\" is not a number.", logs[5][:progname]
      assert_equal Logger::ERROR, logs[6][:severity]
      assert_equal "<=====", logs[6][:progname]
    end
  end

  def test_it_validates_response_body_data_and_logging_error_on_instances_rel_with_type_is_not_array
    contents = '{"children":[{"id":123,"name":"test"}]}'
    response_json("user", "Children", contents) do |file_path|

      get "/users/1/children"

      assert_equal 200,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "GET /users/1/children"', logs[0][:progname]

      assert_equal Logger::ERROR, logs[1][:severity]
      assert_equal "[JSON Schema Invalid HTTP Response Error] GET /users/:id/children =====>", logs[1][:progname]
      assert_equal Logger::ERROR, logs[2][:severity]
      assert_equal "  JSON File Path: #{file_path}", logs[2][:progname]
      assert_equal Logger::ERROR, logs[3][:severity]
      assert_equal "  Response Body: {\"children\":[{\"id\":123,\"name\":\"test\"}]}", logs[3][:progname]
      assert_equal Logger::ERROR, logs[4][:severity]
      assert_equal "  Reason:", logs[4][:progname]
      assert_equal Logger::ERROR, logs[5][:severity]
      assert_equal "    #: failed schema #/definitions/user/links/4/targetSchema: `rel:instances`, but targetSchema type is not `array`. Recommend `rel:self`.", logs[5][:progname]
      assert_equal Logger::ERROR, logs[6][:severity]
      assert_equal "<=====", logs[6][:progname]
    end
  end

  def test_it_errors_response_body_data_and_logging_error_on_cannot_be_parsed
    contents = 'No Content.'
    response_json("user", "List", contents) do |file_path|
      get "/users"
      assert_equal 200,  last_response.status
      assert_equal "application/json;", last_response.headers["Content-Type"]
      assert_equal contents, last_response.body

      logs = @logger.last_logs
      assert_equal Logger::INFO, logs[0][:severity]
      assert_equal '[Handle Response API Server] "GET /users"', logs[0][:progname]

      assert_equal Logger::INFO, logs[1][:severity]
      assert_equal '[HTTP Request Validation] OK', logs[1][:progname]

      assert_equal Logger::ERROR, logs[2][:severity]
      assert_equal "[HTTP Response JSON Parse Error] GET /users =====>", logs[2][:progname]
      assert_equal Logger::ERROR, logs[3][:severity]
      assert_equal "  JSON File Path: #{file_path}", logs[3][:progname]
      assert_equal Logger::ERROR, logs[4][:severity]
      assert_equal "  Response Body: No Content.", logs[4][:progname]
      assert_equal Logger::ERROR, logs[5][:severity]
      assert_equal "<=====", logs[5][:progname]
    end
  end

  def post_json(uri, json)
    post uri, json, "CONTENT_TYPE" => "application/json"
  end

  def schema
    JSON.parse(api_schema)
  end

  def api_schema
    <<-eos
      {
        "$schema": "http://json-schema.org/draft-04/hyper-schema",
        "type": [
          "object"
        ],
        "definitions": {
          "user": {
            "$schema": "http://json-schema.org/draft-04/hyper-schema",
            "title": "User",
            "description": "system user.",
            "stability": "prototype",
            "strictProperties": true,
            "type": [
              "object"
            ],
            "definitions": {
              "id": {
                "description": "unique identifier of user",
                "readOnly": true,
                "example": 12345,
                "type": [
                  "number"
                ]
              },
              "name": {
                "description": "name of user",
                "example": "test",
                "type": [
                  "string"
                ]
              }
            },
            "links": [
              {
                "description": "Create a new user.",
                "href": "/users",
                "method": "POST",
                "rel": "create",
                "schema": {
                  "properties": {
                    "name": {
                      "$ref": "#/definitions/user/definitions/name"
                    }
                  },
                  "type": [
                    "object"
                  ],
                  "required": [
                    "name"
                  ]
                },
                "title": "Create"
              },
              {
                "description": "List existing users.",
                "href": "/users",
                "method": "GET",
                "schema": {
                  "properties": {
                    "date_from": {
                      "description": "when user was created",
                      "format": "date",
                      "example": "2016-07-01",
                      "type": [
                        "string"
                      ]
                    }
                  },
                  "type": [
                    "object"
                  ]
                },
                "targetSchema": {
                  "properties": {
                    "users": {
                      "items": {
                        "$ref": "#/definitions/user"
                      },
                      "type": [
                        "array"
                      ]
                    }
                  },
                  "type": [
                    "object"
                  ],
                  "required": [
                    "users"
                  ]
                },
                "rel": "self",
                "title": "List"
              },
              {
                "description": "Delete an existing user.",
                "href": "/users/{(%23%2Fdefinitions%2Fuser%2Fdefinitions%2Fid)}",
                "method": "DELETE",
                "mediaType": "null",
                "rel": "destroy",
                "title": "Delete"
              },
              {
                "description": "List existing users.",
                "href": "/extra_users",
                "method": "GET",
                "rel": "instances",
                "title": "List"
              },
              {
                "description": "Children existing users.",
                "href": "/users/{(%23%2Fdefinitions%2Fuser%2Fdefinitions%2Fid)}/children",
                "method": "GET",
                "targetSchema": {
                  "properties": {
                    "children": {
                      "items": {
                        "$ref": "#/definitions/user"
                      },
                      "type": [
                        "array"
                      ]
                    }
                  },
                  "type": [
                    "object"
                  ]
                },
                "rel": "instances",
                "title": "Children"
              }
            ],
            "properties": {
              "id": {
                "$ref": "#/definitions/user/definitions/id"
              },
              "name": {
                "$ref": "#/definitions/user/definitions/name"
              }
            }
          }
        },
        "properties": {
          "user": {
            "$ref": "#/definitions/user"
          }
        },
        "title": "test api",
        "description": "test web api",
        "links": [
          {
            "href": "http://localhost/api",
            "rel": "self"
          }
        ],
        "id": "test_api"
      }
    eos
  end

  def temp_file(contents)
    file = Tempfile.new("schema")
    file.write(contents)
    file.size()
    yield(file.path)
  ensure
    file.close
    file.unlink
  end

  def response_json(category, link_title, contents)
    dir_path = @response_data_dir_path.join(category)
    Dir.mkdir(dir_path)
    file_path = dir_path.join("#{link_title}.json")
    File.open(file_path, "w") do |f|
      f.write contents
    end
    yield(file_path)
  ensure
    FileUtils.remove_entry_secure(dir_path)
  end

  def proxy_config
    JsonSchemaCliTools::ProxyConfig.new(@response_data_dir_path.join('proxy_config')) do
      post "/users", static: "user/Create.json", status: 201
      get "/users", static: "user/List.json"
      delete "/users/(\\d+)", status: 204
      get "/extra_users", static: "user/List.json"
      get "/users/(\\d+)/children", static: "user/Children.json"
    end
  end

  class TestLogger < Logger

    def initialize(*args)
      super
      @logs = []
    end

    def last_logs
      @logs
    end

    def add(severity, message = nil, progname = nil, &block)
      @logs.push({ severity: severity, progname: progname })
      super
    end
  end

end
