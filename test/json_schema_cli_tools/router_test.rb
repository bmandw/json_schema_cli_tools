require "test_helper"
require "json_schema_cli_tools/router"

class RouterTest < Minitest::Test

  def test_match_route
    router = JsonSchemaCliTools::Router.new('path/to/static_file')
    route_get_users = router.add("/api/users", "GET")
    route_put_user = router.add("/api/users/(\\d+)", "PUT")
    route_any = router.add("/api/(.*)")

    assert_equal route_get_users, router.match_route("GET", "/api/users")
    assert_equal nil, router.match_route("GET", "/ap/users")
    assert_equal route_put_user, router.match_route("PUT", "/api/users/1")
    assert_equal route_any, router.match_route("DELETE", "/api/users/1")
    assert_equal route_any, router.match_route("GET", "/api/groups")
  end

end
