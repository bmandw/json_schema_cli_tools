require "test_helper"
require "json_schema_cli_tools/command/check_request_command"

module JsonSchemaCliTools

  module Command

    class CheckRequestCommandTest < Minitest::Test
      include TestUtil

      def test_parse!
        argv = [
          'path/to/schema.json',
          'user',
          'Info',
          'path/to/data.json'
        ]
        expect = {
          json_schema_file_path: argv[0],
          schema_category: argv[1],
          api_title: argv[2],
          data_file_path: argv[3],
          query_string: nil
        }
        assert_equal expect, CheckRequestCommand.parse!(argv)
      end

      def test_parse_when_query_string_passed
        argv = [
          'path/to/schema.json',
          'user',
          'Info',
          '-q',
          '?name=1'
        ]
        expect = {
          json_schema_file_path: argv[0],
          schema_category: argv[1],
          api_title: argv[2],
          data_file_path: nil,
          query_string: argv[4]
        }
        assert_equal expect, CheckRequestCommand.parse!(argv)
      end

      def test_execute
        # query string
        temp_file(api_schema) do |schema_path|
          cmd = CheckRequestCommand.new({
            json_schema_file_path: schema_path,
            schema_category: 'user',
            api_title: 'List',
            data_file_path: nil,
            query_string: "http://localhost/api/users?date_from=123"
          })
          ex = assert_raises(ValidateError) do
            cmd.execute
          end
            assert_equal "[ERROR:QUERY_STRING]", ex.memo
          assert_equal "#/date_from: failed schema #/definitions/user/links/1/schema/properties/date_from: 123 is not a valid date.", ex.message
        end

        # request body
        temp_file(api_schema) do |schema_path|
          temp_file('{"name":123}') do |data_path|
            cmd = CheckRequestCommand.new({
              json_schema_file_path: schema_path,
              schema_category: 'user',
              api_title: 'Create',
              data_file_path: data_path,
              query_string: nil
            })
            ex = assert_raises(ValidateError) do
              cmd.execute
            end
            assert_equal "[ERROR]", ex.memo
            assert_equal "#/name: failed schema #/definitions/user/links/0/schema/properties/name: For 'properties/name', 123 is not a string.", ex.message
          end
        end
      end

      def api_schema
        <<-EOS
        {
          "$schema": "http://json-schema.org/draft-04/hyper-schema",
          "type": [
            "object"
          ],
          "definitions": {
            "user": {
              "$schema": "http://json-schema.org/draft-04/hyper-schema",
              "title": "User",
              "description": "system user.",
              "stability": "prototype",
              "strictProperties": true,
              "type": [
                "object"
              ],
              "definitions": {
                "id": {
                  "description": "unique identifier of user",
                  "readOnly": true,
                  "example": 12345,
                  "type": [
                    "number"
                  ]
                },
                "name": {
                  "description": "name of user",
                  "example": "test",
                  "type": [
                    "string"
                  ]
                }
              },
              "links": [
                {
                  "description": "Create a new user.",
                  "href": "/users",
                  "method": "POST",
                  "rel": "create",
                  "schema": {
                    "properties": {
                      "name": {
                        "$ref": "#/definitions/user/definitions/name"
                      }
                    },
                    "type": [
                      "object"
                    ]
                  },
                  "title": "Create"
                },
                {
                  "description": "List existing users.",
                  "href": "/users",
                  "method": "GET",
                  "schema": {
                    "properties": {
                      "date_from": {
                        "description": "when user was created",
                        "format": "date",
                        "example": "2016-07-01",
                        "type": [
                          "string"
                        ]
                      }
                    },
                    "type": [
                      "object"
                    ]
                  },
                  "rel": "instances",
                  "title": "List"
                }
              ],
              "properties": {
                "id": {
                  "$ref": "#/definitions/user/definitions/id"
                },
                "name": {
                  "$ref": "#/definitions/user/definitions/name"
                }
              }
            }
          },
          "properties": {
            "user": {
              "$ref": "#/definitions/user"
            }
          },
          "title": "test api",
          "description": "test web api",
          "links": [
            {
              "href": "http://localhost/api",
              "rel": "self"
            }
          ],
          "id": "test_api"
        }
        EOS
      end
    end

  end

end
