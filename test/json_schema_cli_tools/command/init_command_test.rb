require "test_helper"
require "json_schema_cli_tools/command/init_command"

module JsonSchemaCliTools

  module Command

    class InitCommandTest < Minitest::Test
      include TestUtil

      def test_parse!
        argv = [
          'path/to/schema.json',
          'path/to/proxy_config'
        ]
        expect = {
          json_schema_file_path: argv[0],
          proxy_config_file_path: argv[1]
        }
        assert_equal expect, InitCommand.parse!(argv)
      end

      def test_execute
        temp_file(api_schema) do |schema_path|
          temp_file do |proxy_config_file_path|
            InitCommand.new({
              json_schema_file_path: schema_path,
              proxy_config_file_path: proxy_config_file_path
            }).execute

            expect = [
              "# -*- mode: ruby -*-\n",
              "# vi: set ft=ruby :\n",
              "\n",
              "JsonSchemaCliTools.proxy_configure do\n",
              "  #post '/users', static: 'path/to/user-post.json'\n",
              "  #get '/users', static: 'path/to/user-get.json'\n",
              "  #any '(.*)', proxy: 'http://localhost:8080/'\n",
              "end\n"
            ].join

            File.open(proxy_config_file_path, 'r') do |f|
              contents = f.readlines.join
              assert_equal expect, contents
            end
          end
        end
      end

      def api_schema
        <<-EOS
        {
          "$schema": "http://json-schema.org/draft-04/hyper-schema",
          "type": [
            "object"
          ],
          "definitions": {
            "user": {
              "$schema": "http://json-schema.org/draft-04/hyper-schema",
              "title": "User",
              "description": "system user.",
              "stability": "prototype",
              "strictProperties": true,
              "type": [
                "object"
              ],
              "definitions": {
                "id": {
                  "description": "unique identifier of user",
                  "readOnly": true,
                  "example": 12345,
                  "type": [
                    "number"
                  ]
                },
                "name": {
                  "description": "name of user",
                  "example": "test",
                  "type": [
                    "string"
                  ]
                }
              },
              "links": [
                {
                  "description": "Create a new user.",
                  "href": "/users",
                  "method": "POST",
                  "rel": "create",
                  "schema": {
                    "properties": {
                      "name": {
                        "$ref": "#/definitions/user/definitions/name"
                      }
                    },
                    "type": [
                      "object"
                    ]
                  },
                  "title": "Create"
                },
                {
                  "description": "List existing users.",
                  "href": "/users",
                  "method": "GET",
                  "rel": "instances",
                  "title": "List"
                }
              ],
              "properties": {
                "id": {
                  "$ref": "#/definitions/user/definitions/id"
                },
                "name": {
                  "$ref": "#/definitions/user/definitions/name"
                }
              }
            }
          },
          "properties": {
            "user": {
              "$ref": "#/definitions/user"
            }
          },
          "title": "test api",
          "description": "test web api",
          "links": [
            {
              "href": "http://localhost/api",
              "rel": "self"
            }
          ],
          "id": "test_api"
        }
        EOS
      end
    end

  end

end
