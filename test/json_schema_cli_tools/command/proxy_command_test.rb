require "test_helper"
require "json_schema_cli_tools/command/proxy_command"

module JsonSchemaCliTools

  module Command

    class ProxyCommandTest < Minitest::Test

      def test_parse!
        argv = [
          'path/to/schema.json',
          'path/to/proxy_config'
        ]
        expect = {
          port: 8888,
          static_dir_path: ".",
          dump_traffic: false,
          json_schema_file_path: argv[0],
          proxy_config_file_path: argv[1]
        }
        assert_equal expect, ProxyCommand.parse!(argv)
      end

      def test_parse_when_dump_traffic_on
        argv = [
          'path/to/schema.json',
          'path/to/proxy_config',
          '-d'
        ]
        expect = {
          port: 8888,
          static_dir_path: ".",
          dump_traffic: true,
          json_schema_file_path: argv[0],
          proxy_config_file_path: argv[1]
        }
        assert_equal expect, ProxyCommand.parse!(argv)
      end

      def test_parse_when_port_passed
        argv = [
          'path/to/schema.json',
          'path/to/proxy_config',
          '-p',
          '8080'
        ]
        expect = {
          port: 8080,
          static_dir_path: ".",
          dump_traffic: false,
          json_schema_file_path: argv[0],
          proxy_config_file_path: argv[1]
        }
        assert_equal expect, ProxyCommand.parse!(argv)
      end

      def test_parse_when_static_dir_path_passed
        argv = [
          'path/to/schema.json',
          'path/to/proxy_config',
          '-s',
          'path/to/static'
        ]
        expect = {
          port: 8888,
          static_dir_path: argv[3],
          dump_traffic: false,
          json_schema_file_path: argv[0],
          proxy_config_file_path: argv[1]
        }
        assert_equal expect, ProxyCommand.parse!(argv)
      end

    end

  end

end
