require "test_helper"
require "json_schema_cli_tools/command/check_response_command"

module JsonSchemaCliTools

  module Command

    class CheckResponseCommandTest < Minitest::Test
      include TestUtil

      def test_parse!
        argv = [
          'path/to/schema.json',
          'user',
          'Info',
          'path/to/data.json'
        ]
        expect = {
          json_schema_file_path: argv[0],
          schema_category: argv[1],
          api_title: argv[2],
          data_file_path: argv[3]
        }
        assert_equal expect, CheckResponseCommand.parse!(argv)
      end

      def test_execute
        temp_file(api_schema) do |schema_path|
          temp_file('[{"name":"test"}]') do |data_path|
            cmd = CheckResponseCommand.new({
              json_schema_file_path: schema_path,
              schema_category: 'user',
              api_title: 'List',
              data_file_path: data_path
            })
            ex = assert_raises(ValidateError) do
              cmd.execute
            end
            assert_equal "[ERROR]", ex.memo
            assert_equal "#: failed schema #/definitions/user: \"id\" wasn't supplied.", ex.message
          end
        end
      end

      def api_schema
        <<-EOS
        {
          "$schema": "http://json-schema.org/draft-04/hyper-schema",
          "type": [
            "object"
          ],
          "definitions": {
            "user": {
              "$schema": "http://json-schema.org/draft-04/hyper-schema",
              "title": "User",
              "description": "system user.",
              "stability": "prototype",
              "strictProperties": true,
              "type": [
                "object"
              ],
              "definitions": {
                "id": {
                  "description": "unique identifier of user",
                  "readOnly": true,
                  "example": 12345,
                  "type": [
                    "number"
                  ]
                },
                "name": {
                  "description": "name of user",
                  "example": "test",
                  "type": [
                    "string"
                  ]
                }
              },
              "links": [
                {
                  "description": "Create a new user.",
                  "href": "/users",
                  "method": "POST",
                  "rel": "create",
                  "schema": {
                    "properties": {
                      "name": {
                        "$ref": "#/definitions/user/definitions/name"
                      }
                    },
                    "type": [
                      "object"
                    ]
                  },
                  "title": "Create"
                },
                {
                  "description": "List existing users.",
                  "href": "/users",
                  "method": "GET",
                  "schema": {
                    "properties": {
                      "date_from": {
                        "description": "when user was created",
                        "format": "date",
                        "example": "2016-07-01",
                        "type": [
                          "string"
                        ]
                      }
                    },
                    "type": [
                      "object"
                    ]
                  },
                  "rel": "instances",
                  "title": "List"
                }
              ],
              "properties": {
                "id": {
                  "$ref": "#/definitions/user/definitions/id"
                },
                "name": {
                  "$ref": "#/definitions/user/definitions/name"
                }
              }
            }
          },
          "properties": {
            "user": {
              "$ref": "#/definitions/user"
            }
          },
          "title": "test api",
          "description": "test web api",
          "links": [
            {
              "href": "http://localhost/api",
              "rel": "self"
            }
          ],
          "id": "test_api"
        }
        EOS
      end
    end

  end

end
