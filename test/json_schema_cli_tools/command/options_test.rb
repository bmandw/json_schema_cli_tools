require "test_helper"
require "json_schema_cli_tools/command/options"

module JsonSchemaCliTools

  module Command

    class OptionsTest < Minitest::Test

      def test_parse_when_init_sub_command
        argv = [
          'init',
          'path/to/schema.json',
          'path/to/proxy_config'
        ]
        expect = {
          json_schema_file_path: argv[1],
          proxy_config_file_path: argv[2]
        }
        sub_command, options = Options.parse!(argv)
        assert_equal InitCommand, sub_command
        assert_equal expect, options
      end

      def test_parse_when_proxy_sub_command
        argv = [
          'proxy',
          'path/to/schema.json',
          'path/to/proxy_config'
        ]
        expect = {
          port: 8888,
          static_dir_path: ".",
          dump_traffic: false,
          json_schema_file_path: argv[1],
          proxy_config_file_path: argv[2]
        }
        sub_command, options = Options.parse!(argv)
        assert_equal ProxyCommand, sub_command
        assert_equal expect, options
      end

      def test_parse_when_check_request_sub_command
        argv = [
          'check-request',
          'path/to/schema.json',
          'user',
          'Info',
          'path/to/data.json'
        ]
        expect = {
          json_schema_file_path: argv[1],
          schema_category: argv[2],
          api_title: argv[3],
          data_file_path: argv[4],
          query_string: nil
        }
        sub_command, options = Options.parse!(argv)
        assert_equal CheckRequestCommand, sub_command
        assert_equal expect, options
      end

      def test_parse_when_check_response_sub_command
        argv = [
          'check-response',
          'path/to/schema.json',
          'user',
          'Info',
          'path/to/data.json'
        ]
        expect = {
          json_schema_file_path: argv[1],
          schema_category: argv[2],
          api_title: argv[3],
          data_file_path: argv[4]
        }
        sub_command, options = Options.parse!(argv)
        assert_equal CheckResponseCommand, sub_command
        assert_equal expect, options
      end

    end

  end

end
