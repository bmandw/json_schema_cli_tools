require "test_helper"
require "json_schema_cli_tools/route"

module JsonSchemaCliTools

  class RouteTest < Minitest::Test

    def test_match?
      route_get = Route.new("/api/users", "GET")
      assert_equal true,  route_get.match?("/api/users", "GET")
      assert_equal false, route_get.match?("/api/users/1", "GET")
      assert_equal false, route_get.match?("/api/users", "POST")
      assert_equal false, route_get.match?("/api/usrs", "GET")

      route_any = Route.new("/api/users")
      assert_equal true,  route_any.match?("/api/users", "GET")
      assert_equal false, route_any.match?("/api/users/1", "GET")
      assert_equal true,  route_any.match?("/api/users", "POST")
      assert_equal false, route_any.match?("/api/users/1", "PUT")
      assert_equal false, route_any.match?("/api/usrs", "GET")

      route_put = Route.new("/api/users/(\\d+)", "PUT")
      assert_equal true,  route_put.match?("/api/users/1", "PUT")
      assert_equal false, route_put.match?("/api/users/1", "GET")
      assert_equal false, route_put.match?("/api/users/1/group/2", "PUT")
      assert_equal false, route_put.match?("/api/users/", "PUT")
      assert_equal false, route_put.match?("/api/users", "PUT")
    end

    def test_status_code
      route = Route.new("/users", "GET")
      assert_equal 200, route.status_code

      route_201 = Route.new("/users", "GET", { status: 201 })
      assert_equal 201, route_201.status_code
    end

    def test_static_response?
      route_static = Route.new("/users", "GET", { static: 'path/to/json'})
      assert route_static.static_response?

      route_204 = Route.new("/users", "GET", { status: 204})
      assert route_204.static_response?

      route_proxy = Route.new("/users", "GET", { proxy: 'http://localhost'})
      refute route_proxy.static_response?

      route_undefined = Route.new("/users", "GET")
      refute route_undefined.static_response?
    end

    def test_capture_params
      route1 = Route.new("/users/(\\d+)", "GET")
      assert_equal ["12345"], route1.capture_params("/users/12345")

      route2 = Route.new("/users/(\\d+)/groups/(.*)", "GET")
      assert_equal ["12345", "98765"], route2.capture_params("/users/12345/groups/98765")

      route3 = Route.new("/users", "GET")
      assert_equal [], route3.capture_params("/users")

      unmatch_route = Route.new("/users", "GET")
      assert_equal nil, unmatch_route.capture_params("/users/1234")
    end

    def test_rewritten_path
      route1 = Route.new("/api/users", "GET", { rewrite: {"^/api"=>"/MyApp/api"}})
      assert_equal "/MyApp/api/users", route1.rewritten_path

      route2 = Route.new("/users", "GET", { rewrite: {"^/api"=>"/MyApp/api"}})
      assert_equal "/users", route2.rewritten_path

      route3 = Route.new("/test/api/users", "GET", { rewrite: {"^/api"=>"/MyApp/api", "^/test/"=>"/"}})
      assert_equal "/api/users", route3.rewritten_path
    end

    def test_rewrite
      route1 = Route.new("/api/users/(\\d+)", "GET", { rewrite: {"^/api"=>"/MyApp/api"}})
      assert_equal "/MyApp/api/users/1", route1.rewrite("/api/users/1")

      assert_equal "/users", route1.rewrite("/users")

      route2 = Route.new("/api/users", "GET", { rewrite: {"^/api"=>"/MyApp/api", "^/test/"=>"/"}})
      assert_equal "/api/users", route2.rewrite("/test/api/users")
    end

  end

end
