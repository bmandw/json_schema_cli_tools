require "test_helper"
require "json_schema_cli_tools/proxy_config"

class ProxyConfigTest < Minitest::Test

  def test_initialize
    JsonSchemaCliTools::ProxyConfig.new('config/proxy') do
      get '/api/users', static: 'path/to/users.json'
      post '/api/users', static: 'path/to/create_user.json'
      put '/api/users/(\d+)', static: 'path/to/update_user.json'
      delete '/api/users/(\d+)', static: 'path/to/remove_user.json'
      any '/api/(.*)', proxy: 'http://localhost:8080/MyApp/api\1'
    end
  end

end
