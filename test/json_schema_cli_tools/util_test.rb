require "test_helper"
require "json_schema"
require "json_schema_cli_tools/util"

class UtilTest < Minitest::Test

  def test_query_string_to_hash
    result = JsonSchemaCliTools::Util.query_string_to_hash('a=1&b=2&c[]=3&c[]=4')
    assert_equal result, {
      "a" => "1",
      "b" => "2",
      "c" => ["3", "4"]
    }
  end

  def test_create_json_schema_invalid_type_error
    e = JsonSchemaCliTools::Util.create_json_schema_invalid_type_error(schema, "error!!!")
    assert_equal e.to_s, "#: failed schema #: error!!!"
  end

  def schema
    JsonSchema.parse!(schema_sample)
  end

  def schema_sample
    {
      "$schema" => "http://json-schema.org/draft-04/hyper-schema",
      "type" => [
        "object"
      ],
      "definitions" => {
        "user" => {
          "$schema" => "http://json-schema.org/draft-04/hyper-schema",
          "title" => "User",
          "description" => "system user.",
          "stability" => "prototype",
          "strictProperties" => true,
          "type" => [
            "object"
          ],
          "definitions" => {
            "id" => {
              "description" => "unique identifier of user",
              "readOnly" => true,
              "example" => 12345,
              "type" => [
                "number"
              ]
            },
            "name" => {
              "description" => "name of user",
              "example" => "test",
              "type" => [
                "string"
              ]
            }
          },
          "links" => [
            {
              "description" => "List existing users.",
              "href" => "/users",
              "method" => "GET",
              "rel" => "instances",
              "title" => "List"
            }
          ],
          "properties" => {
            "id" => {
              "$ref" => "#/definitions/user/definitions/id"
            },
            "name" => {
              "$ref" => "#/definitions/user/definitions/name"
            }
          }
        }
      },
      "properties" => {
        "user" => {
          "$ref" => "#/definitions/user"
        }
      },
      "title" => "test api",
      "description" => "test web api",
      "links" => [
        {
          "href" => "http://localhost/api",
          "rel" => "self"
        }
      ],
      "id" => "test_api"
    }
  end

end
