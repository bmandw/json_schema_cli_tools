require "test_helper"
require "json"
require "json_schema"
require "json_schema_cli_tools/validator"

module JsonSchemaCliTools

  class ValidatorTest < Minitest::Test

    def setup
      @schema = json_schema
    end

    def test_find_link_validator_when_undefined_link
      assert_instance_of Validator, Validator.find_link_validator(@schema, 'user', 'List')
    end

    def test_find_link_validator_when_undefined_link
      ex = assert_raises(ArgumentError) do
        Validator.find_link_validator(@schema, 'u', 'List')
      end
      assert_equal [
        "u: No such category schema.\n",
        "[Available] user\n"
      ].join, ex.message

      ex = assert_raises(ArgumentError) do
        Validator.find_link_validator(@schema, 'user', 'L')
      end
      assert_equal [
        "L: No such link title in `user` category schema.\n",
        "[Available] Create|List|Delete|ExtraList|Children\n"
      ].join, ex.message
    end

    def test_validate_request_query_string
      validator = Validator.find_link_validator(@schema, 'user', 'List')
      ex = assert_raises(ValidateError) do
        validator.validate_request_query_string("?date_from=123")
      end
      assert_equal "[ERROR:QUERY_STRING]", ex.memo
      assert_equal "#/date_from: failed schema #/definitions/user/links/1/schema/properties/date_from: 123 is not a valid date.", ex.message

      validator.validate_request_query_string("?date_from=2016-12-31")
    end

    def test_validate_request_query_string_params
      validator = Validator.find_link_validator(@schema, 'user', 'List')
      ex = assert_raises(ValidateError) do
        query_params = Util.query_string_to_hash(URI.parse("?date_from=123").query)
        validator.validate_request_query_string_params(query_params)
      end
      assert_equal "[ERROR:QUERY_STRING]", ex.memo
      assert_equal "#/date_from: failed schema #/definitions/user/links/1/schema/properties/date_from: 123 is not a valid date.", ex.message

      validator.validate_request_query_string("?date_from=2016-12-31")
    end


    def test_validate_request_body
      validator = Validator.find_link_validator(@schema, 'user', 'Create')
      ex = assert_raises(ValidateError) do
        validator.validate_request_body({})
      end
      assert_equal "[ERROR]", ex.memo
      assert_equal "#: failed schema #/definitions/user/links/0/schema: \"name\" wasn't supplied.", ex.message

      validator.validate_request_body({"name"=>"test"})
    end

    def test_validate_response_body
      validator = Validator.find_link_validator(@schema, 'user', 'Create')
      ex = assert_raises(ValidateError) do
        validator.validate_response_body({})
      end
      assert_equal "[ERROR]", ex.memo
      assert_equal "#: failed schema #/definitions/user: \"id\", \"name\" weren't supplied.", ex.message

      validator.validate_response_body({"id"=>123, "name"=>"test"})
    end

    def test_validate_response_body_when_list_data
      validator = Validator.find_link_validator(@schema, 'user', 'ExtraList')
      ex = assert_raises(ValidateError) do
        validator.validate_response_body([
          {"id" => 1}
        ])
      end
      assert_equal "[ERROR]", ex.memo
      assert_equal "#: failed schema #/definitions/user: \"name\" wasn't supplied.", ex.message

      validator.validate_response_body([
        {"id"=>123, "name"=>"test"}
      ])
    end

    def test_validate_response_body_when_defined_instances_miss
      validator = Validator.find_link_validator(@schema, 'user', 'Children')
      ex = assert_raises(InstancesRelTypeError) do
        validator.validate_response_body([
          {"id" => 1}
        ])
      end
      assert_equal "[ERROR]", ex.memo
      assert_equal "#: failed schema #/definitions/user/links/4/targetSchema: `rel:instances`, but targetSchema type is not `array`. Recommend `rel:self`.", ex.message
    end

    def json_schema
      JsonSchema.parse!(JSON.parse(api_schema))
    end

    def api_schema
      <<-EOS
      {
        "$schema": "http://json-schema.org/draft-04/hyper-schema",
        "type": [
          "object"
        ],
        "definitions": {
          "user": {
            "$schema": "http://json-schema.org/draft-04/hyper-schema",
            "title": "User",
            "description": "system user.",
            "stability": "prototype",
            "strictProperties": true,
            "type": [
              "object"
            ],
            "definitions": {
              "id": {
                "description": "unique identifier of user",
                "readOnly": true,
                "example": 12345,
                "type": [
                  "number"
                ]
              },
              "name": {
                "description": "name of user",
                "example": "test",
                "type": [
                  "string"
                ]
              }
            },
            "links": [
              {
                "description": "Create a new user.",
                "href": "/users",
                "method": "POST",
                "rel": "create",
                "schema": {
                  "properties": {
                    "name": {
                      "$ref": "#/definitions/user/definitions/name"
                    }
                  },
                  "type": [
                    "object"
                  ],
                  "required": [
                    "name"
                  ]
                },
                "title": "Create"
              },
              {
                "description": "List existing users.",
                "href": "/users",
                "method": "GET",
                "schema": {
                  "properties": {
                    "date_from": {
                      "description": "when user was created",
                      "format": "date",
                      "example": "2016-07-01",
                      "type": [
                        "string"
                      ]
                    }
                  },
                  "type": [
                    "object"
                  ]
                },
                "targetSchema": {
                  "properties": {
                    "users": {
                      "items": {
                        "$ref": "#/definitions/user"
                      },
                      "type": [
                        "array"
                      ]
                    }
                  },
                  "type": [
                    "object"
                  ],
                  "required": [
                    "users"
                  ]
                },
                "rel": "self",
                "title": "List"
              },
              {
                "description": "Delete an existing user.",
                "href": "/users/{(%23%2Fdefinitions%2Fuser%2Fdefinitions%2Fid)}",
                "method": "DELETE",
                "mediaType": "null",
                "rel": "destroy",
                "title": "Delete"
              },
              {
                "description": "List existing users.",
                "href": "/extra_users",
                "method": "GET",
                "rel": "instances",
                "title": "ExtraList"
              },
              {
                "description": "Children existing users.",
                "href": "/users/{(%23%2Fdefinitions%2Fuser%2Fdefinitions%2Fid)}/children",
                "method": "GET",
                "targetSchema": {
                  "properties": {
                    "children": {
                      "items": {
                        "$ref": "#/definitions/user"
                      },
                      "type": [
                        "array"
                      ]
                    }
                  },
                  "type": [
                    "object"
                  ]
                },
                "rel": "instances",
                "title": "Children"
              }
            ],
            "properties": {
              "id": {
                "$ref": "#/definitions/user/definitions/id"
              },
              "name": {
                "$ref": "#/definitions/user/definitions/name"
              }
            }
          }
        },
        "properties": {
          "user": {
            "$ref": "#/definitions/user"
          }
        },
        "title": "test api",
        "description": "test web api",
        "links": [
          {
            "href": "http://localhost/api",
            "rel": "self"
          }
        ],
        "id": "test_api"
      }
      EOS
    end

  end

end
