require "uri"
require "rack"
require "json"

module JsonSchemaCliTools

  module Util

    def self.query_string_to_hash(query_string)
      shorten_key_names(Rack::Utils.parse_query(query_string))
    end

    def self.shorten_key_names(data)
      result = {}
      data.each do |key, value|
        key = key.sub(/\[\]/, '')
        result[key] = value
      end
      result
    end

    def self.create_json_schema_invalid_type_error(schema, message, path='#')
      JsonSchema::AggregateError.new [
        JsonSchema::ValidationError.new(
          schema,
          [path],
          message,
          :invalid_type
        )
      ]
    end

  end

end
