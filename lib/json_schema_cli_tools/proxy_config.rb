require "json_schema_cli_tools/router"

module JsonSchemaCliTools

  class ProxyConfig

    CONFIGURE_MUTEX = Mutex.new

    SUPPORTED_HTTP_METHODS = %w{GET POST PUT DELETE HEAD TRACE PATCH OPTIONS LINK UNLINK}

    attr_reader :router

    def self.load(proxy_config_file_path)
      proxy_config_file_path = File.expand_path(proxy_config_file_path)
      @last_load_file_path = [proxy_config_file_path]
      self.capture_configure do
        begin
          Kernel.load proxy_config_file_path
        rescue
          print "Proxy Config Not Found.\n"
        end
      end
    end

    def self.define(&block)
      @last_procs ||= []
      config_file_path = @last_load_file_path.pop
      @last_procs << self.new(config_file_path, &block)
    end

    def self.capture_configure
      CONFIGURE_MUTEX.synchronize do
        @last_procs = []
        yield
        return @last_procs.pop
      end
    end

    def initialize(config_file_path, &block)
      @router = Router.new(File.dirname(config_file_path))
      define(&block)
    end

    def proxy_config_dir_path
      File.dirname(@proxy_config_file_path)
    end

    def define(&block)
      instance_eval(&block) if block_given?
    end

    SUPPORTED_HTTP_METHODS.each do |request_method|
      request_method_symbol = request_method.downcase.to_sym
      define_method(request_method_symbol) do |path, options = {}|
        @router.add(path, request_method, options)
      end
    end

    def any(path, options = {})
      @router.add(path, nil, options)
    end

  end

end
