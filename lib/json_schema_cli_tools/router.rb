require "json_schema_cli_tools/route"

module JsonSchemaCliTools

  class Router

    def initialize(config_root_path)
      @routes = []
      @config_root_path = config_root_path
    end

    def add(path, request_method=nil, options={})
      add_route(Route.new(path, request_method, options.merge({
        config_root_path: @config_root_path
      })))
    end

    def add_route(route)
      @routes << route
      route
    end

    def match_route(request_method, href)
      @routes.find do |r|
        r.match?(href, request_method)
      end
    end

  end

end
