require "cgi"
require "pathname"
require "json"
require "json_schema_cli_tools/util"
require "json_schema_cli_tools/query_params_conversion"

module JsonSchemaCliTools

  class ValidationServer

    def initialize(app, schema: nil, logger: nil, response_generator: nil, dump_traffic: nil)
      @app = app
      @logger = logger
      @response_generator = response_generator
      @schema = Rack::JsonSchema::Schema.new(schema)
      @dump_traffic = dump_traffic
    end

    def call(env)
      RequestHandler.call(app: @app, env: env, schema: @schema, logger: @logger, response_generator: @response_generator, dump_traffic: @dump_traffic)
    end

    class RequestHandler < Rack::JsonSchema::BaseRequestHandler

      def initialize(app: nil, logger: nil, response_generator: nil, dump_traffic: nil,  **args)
        super(**args)
        @app = app
        @logger = logger
        @response_generator = response_generator
        @dump_traffic = dump_traffic
      end

      def call
        handle_action_name = @response_generator.handle_action_name

        if has_link_for_current_action?
          @logger.info "[#{handle_action_name}] \"#{method} #{path}#{query_string}\""
          dump_request
          validate_request
          dump_response_if_needed validate_response
        else
          @logger.info "[Handle Local Static File] \"#{method} #{path}#{query_string}\""
          @app.call(@env)
        end
      end

      private

      def query_string
        val = @env["QUERY_STRING"]
        if val.empty?
          val
        else
          "?#{val}"
        end
      end

      def href_path
        link.href.gsub(/{(.+?)}/) do
          ":" + CGI.unescape($1).gsub(/[()\s]/, "").split("/").last
        end
      end

      def params
        Util::shorten_key_names(request.params)
      end

      def dump_request
        if @dump_traffic then
          raw_data = request.body.read || ''
          request.body.rewind
          unless raw_data.empty? then
            @logger.info "[Request Body] #{raw_data}"
          end
        end
      end

      def dump_response_if_needed(response)
        if @dump_traffic then
          body = response[2]
          raw_data = body.join() || ''
          if raw_data.encoding != Encoding::ASCII_8BIT then
            # バイナリでなければ表示
            unless raw_data.empty? then
              @logger.info "[Response Body] #{raw_data}"
            end
          end
        end
        response
      end

      def validate_request
        unless link.schema.nil?
          if link.method == :get
            validate_request_query_string
          else
            validate_request_body
          end
        end
      end

      def validate_request_query_string
          begin
            Validator.new(link).validate_request_query_string_params(params)
            @logger.info "[HTTP Request Validation] OK"
          rescue => e
            @logger.error "[JSON Schema Invalid HTTP Request QueryString Error] #{method} #{href_path} =====>"
            @logger.error "  QueryString Params: #{JSON.generate(params)}"
            @logger.error "  Reason:"
            e.errors.each do |error|
              @logger.error "    #{error.to_s}"
            end
            @logger.error "<====="
          end
      end

      def validate_request_body
          begin
            raw_data = request.body.read
            data = JSON.parse(raw_data)
            Validator.new(link).validate_request_body(data)
            @logger.info "[HTTP Request Validation] OK"
          rescue => e
            @logger.error "[JSON Schema Invalid HTTP Request Error] #{method} #{href_path} =====>"
            @logger.error "  Request Body: #{raw_data}"
            @logger.error "  Reason:"
            e.errors.each do |error|
              @logger.error "    #{error.to_s}"
            end
            @logger.error "<====="
          end
      end

      def has_response_json_body?
        link.media_type != "null" && link.media_type == "application/json"
      end

      def validate_response
        status, headers, body, meta = @response_generator.generate(@app, @env, link)

        content_type = headers["Content-Type"] || ""
        media_type = link.media_type

        if media_type != "null" && !content_type.start_with?(media_type)
          @logger.warn "[HTTP Response Header Miss] mediaType:`#{media_type}` != Content-Type:`#{content_type}`"
        end

        if status == 204 && media_type != "null"
          @logger.warn "204 status code. [Recommend] mediaType:null"
        end

        if has_response_json_body?
          begin
            raw_data =  body.join()
            data = JSON.parse(raw_data)
            Validator.new(link).validate_response_body(data)
            @logger.info "[HTTP Response Validation] OK"
          rescue JSON::ParserError => e
            @logger.error "[HTTP Response JSON Parse Error] #{method} #{href_path} =====>"
            @logger.error "  #{meta}"
            if raw_data.encoding != Encoding::ASCII_8BIT then
              # バイナリでなければ表示
              @logger.error "  Response Body: #{raw_data}"
            end
            @logger.error "<====="
          rescue ValidateError, InstancesRelTypeError => e
            @logger.error "[JSON Schema Invalid HTTP Response Error] #{method} #{href_path} =====>"
            @logger.error "  #{meta}"
            @logger.error "  Response Body: #{JSON.generate(data)}"
            @logger.error "  Reason:"
            e.errors.each do |error|
              @logger.error "    #{error.to_s}"
            end
            @logger.error "<====="
          end
        end
        [status, headers, body]
      end

    end

  end

end
