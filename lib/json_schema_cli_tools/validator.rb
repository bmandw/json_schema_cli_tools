require "json_schema_cli_tools/error"

module JsonSchemaCliTools

  class Validator

    def self.find_link_validator(schema, schema_category, title)
      category_schema = schema.definitions[schema_category]
      if category_schema.nil?
        raise ArgumentError, <<-EOS.gsub(/^\s+/, '')
          #{schema_category}: No such category schema.
          [Available] #{schema.definitions.keys().join('|')}
        EOS
      end

      link = category_schema.links.find {|l| l.title == title }
      if link.nil?
        link_titles = category_schema.links.map {|l| l.title }
        raise ArgumentError, <<-EOS.gsub(/^\s+/, '')
          #{title}: No such link title in `#{schema_category}` category schema.
          [Available] #{link_titles.join('|')}
        EOS
      end

      self.new(link)
    end

    def initialize(link)
      @link = link
    end

    def validate_request_query_string(query_string)
      query_params = Util.query_string_to_hash(URI.parse(query_string).query)
      validate_request_query_string_params(query_params)
    end

    def validate_request_query_string_params(query_params)
      unless @link.schema.nil?
        begin
          converted_data = QueryParamsConversion.new(@link.schema).convert(query_params)
          @link.schema.validate!(converted_data)
        rescue => e
          raise ValidateError.new(e.errors, '[ERROR:QUERY_STRING]')
        end
      end
    end

    def validate_request_body(json_data)
      unless @link.schema.nil?
        begin
          @link.schema.validate!(json_data)
        rescue => e
          raise ValidateError.new(e.errors)
        end
      end
    end

    def validate_response_body(json_data)
      begin
        if @link.rel == "instances"
          if @link.target_schema && @link.target_schema.type != "array"
            raise InstancesRelTypeError.new(@link)
          end
          json_data.each do |inner|
            response_schema.validate!(inner)
          end
        else
          response_schema.validate!(json_data)
        end
      rescue JsonSchema::AggregateError => e
        raise ValidateError.new(e.errors)
      end
    end

    private

    def response_schema
      @link.target_schema || @link.parent
    end

  end

end
