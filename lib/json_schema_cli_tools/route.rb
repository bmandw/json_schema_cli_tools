module JsonSchemaCliTools

  class Route

    attr_reader :path, :request_method

    def initialize(path, request_method=nil, options={})
      @path = path
      @request_method = request_method
      @options = options
      @matcher = Regexp.new("^#{path}$")
    end

    def match?(path, request_method)
      return false if request_method != @request_method && !@request_method.nil?
      return !@matcher.match(path).nil?
    end

    def capture_params(path)
      matched = @matcher.match(path)
      return matched.captures unless matched.nil?
    end

    def status_code
      @options[:status] || 200
    end

    def static_response?
      @options.include?(:static) || @options[:status] == 204
    end

    def proxy_response?
      @options.include?(:proxy)
    end

    def erb_response?
      @options.include?(:erb)
    end

    def static_file_path
      File.join(@options[:config_root_path], @options[:static])
    end

    def proxy_server_uri
      @options[:proxy]
    end

    def proxy_rewrite
      @rewrite_map ||= generate_rewrite_map(@options[:rewrite] || {})
    end

    def rewritten_path
      @rewritten_path ||= rewrite(path)
    end

    def rewrite(path)
      replaced = path
      proxy_rewrite.each do |k, v|
        if path.match(k)
          replaced = path.gsub(k, v)
          break
        end
      end
     replaced
    end

    def generate_rewrite_map(map)
      map.inject({}) do |r, (k, v)|
        r[Regexp.new(k)] = v
        r
      end
    end

    def erb_file_path
      File.join(@options[:config_root_path], @options[:erb])
    end

  end

end
