require "rack/reverse_proxy"

module JsonSchemaCliTools

  class ExtRoundTrip < RackReverseProxy::RoundTrip

    def can_have_body?
      if target_request.method == 'DELETE'
        # DELETEメソッドでもbodyを持てるように拡張
        true
      else
        target_request.request_body_permitted?
      end
    end

  end

end
