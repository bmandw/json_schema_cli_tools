require "json_schema_cli_tools/util"

module JsonSchemaCliTools

  class QueryParamsConversion

    def initialize(schema)
      @schema = schema
    end

    def convert(data)
      convert_properties(@schema, data, {})
    end

    private

    def convert_properties(schema, data, result)
      unless schema.properties.empty?
        schema.properties.each do |key, subschema|
          if data.key?(key)
            result[key] = convert_data(subschema, data[key])
          end
        end
      end
      result
    end

    def convert_data(schema, data)
      type = schema.type
      if type.include?("null") && data.empty?
        nil
      elsif type.include?("string")
        convert_string(schema, data)
      elsif type.include?("boolean")
        convert_boolean(schema, data)
      elsif type.include?("integer")
        convert_integer(schema, data)
      elsif type.include?("number")
        convert_number(schema, data)
      elsif type.include?("array")
        convert_array(schema, data)
      end
    end

    def convert_boolean(schema, value)
      if value == "true"
        true
      elsif value == "false"
        false
      else
        raise JsonSchemaCliTools::Util.create_json_schema_invalid_type_error(schema, "boolean must be true or false")
      end
    end

    def convert_string(schema, value)
      value
    end

    def convert_integer(schema, value)
      begin
        Integer(value)
      rescue ArgumentError => e
        raise JsonSchemaCliTools::Util.create_json_schema_invalid_type_error(schema, e.to_s)
      end
    end

    def convert_number(schema, value)
      begin
        Float(value)
      rescue ArgumentError => e
        raise JsonSchemaCliTools::Util.create_json_schema_invalid_type_error(schema, e.to_s)
      end
    end

    def convert_array(schema, data)
      data = data.is_a?(Array) ? data : [data]
      data.map do |d|
        convert_data(schema.items, d)
      end
    end

  end

end
