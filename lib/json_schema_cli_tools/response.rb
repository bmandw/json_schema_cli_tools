require "rack/reverse_proxy"
require "json_schema_cli_tools/ext_roundtrip"
require "erb"
require "uri"

module JsonSchemaCliTools

  class Response

    attr_reader :handle_action_name

    def initialize(proxy_config)
      @handle_action_name = "Handle Response API Server"
      @proxy_config = proxy_config
    end

    def generate(app, env, link)
      request_path = env["REQUEST_PATH"] || env["PATH_INFO"]
      request_method = env["REQUEST_METHOD"]
      route = @proxy_config.router.match_route(request_method, request_path)
      unless route.nil?
        if route.erb_response?
          respond_erb(app, env, link, route)
        elsif route.static_response?
          respond_file(app, env, link, route)
        elsif route.proxy_response?
          respond_proxy(app, env, link, route)
        end
      else
        [502, {}, ["Bad Gateway"], "Route not found in proxy config"]
      end
    end

    def respond_proxy(app, env, link, route)
      api_server_uri = route.proxy_server_uri

      # rewriteするために書き換え
      rewritten_env = env.dup
      rewritten_env["PATH_INFO"] = route.rewrite(rewritten_env["PATH_INFO"])
      rewritten_env["REQUEST_PATH"] = route.rewrite(rewritten_env["REQUEST_PATH"])

      rules = [
        RackReverseProxy::Rule.new(Regexp.new(route.rewritten_path), "#{api_server_uri}", {})
      ]
      status, header, body = ExtRoundTrip.new(app, rewritten_env, {
        :preserve_host => true,
        :preserve_encoding => false,
        :x_forwarded_headers => true,
        :matching => :all,
        :replace_response_host => false
      }, rules).call
      header.delete("Etag") # プロキシする対象に304ステータスを起こさせないようにEtagを消しておく
      [status, header, [body.to_s], "API Server: #{api_server_uri}"]
    end

    def respond_file(app, env, link, route)
      if route.status_code == 204
        [204, {}, [], nil]
      else
        if File.exist?(route.static_file_path)
          raw_data = ::File.read(route.static_file_path)
          [route.status_code, {"Content-Type" => "#{link.media_type};"}, [raw_data], "JSON File Path: #{route.static_file_path}"]
        else
          [404, {}, ["Not Found"], "JSON File Path (Not Found): #{route.static_file_path}"]
        end
      end
    end

    def respond_erb(app, env, link, route)
      if File.exist?(route.erb_file_path)
        template_content = File.open(route.erb_file_path, 'r').readlines.join
        template = ERB.new(template_content, nil, '-')

        request = Rack::Request.new(env)
        request_body = request.body
        request_body.rewind
        raw_body = request_body.read
        params = request.params
        body = JSON.parse(raw_body) unless raw_body.empty?

        request_path = env["REQUEST_PATH"] || env["PATH_INFO"]
        args = route.capture_params(request_path)
        context = -> {
          args = args
          params = params
          body = body
          binding
        }
        raw_data = template.result(context.call())
        [route.status_code, {"Content-Type" => "#{link.media_type};"}, [raw_data], "ERB File Path: #{route.erb_file_path}"]
      else
        [404, {}, ["Not Found"], "ERB File Path (Not Found): #{route.erb_file_path}"]
      end
    end

  end

end
