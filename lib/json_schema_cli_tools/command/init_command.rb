require "optparse"
require "json_schema"
require "erb"
require "json_schema_cli_tools/command/mixin"

module JsonSchemaCliTools

  module Command

    class InitCommand
      include Mixin

      def self.print_usage!
        $stderr.puts "Usage: jsct init <schema_file_path>"
        $stderr.puts "       jsct init <schema_file_path> <proxy_config_file_path>"
      end

      def self.default_options
        {
          json_schema_file_path: "schema.json",
          proxy_config_file_path: "proxy_config"
        }
      end

      def self.parse!(argv)
        options = default_options
        OptionParser.new { |opts|
          opts.on("-h", "--help",) do |s|
            print_usage!
            exit
          end
        }.parse!(argv)
        options[:json_schema_file_path] = argv.shift
        options[:proxy_config_file_path] = argv.shift
        options
      end

      def initialize(options)
        @options = options
      end

      def execute
        schema = read_json_file(@options[:json_schema_file_path])
        json_schema = JsonSchema.parse!(schema)

        routes = []
        json_schema.definitions.each do |k, v|
          v.links.each do |l|
            href = l.href.gsub(/{(.+?)}/) do
              "(.*)"
            end
            routes << [l.method.id2name, href, k]
          end
        end

        proxy_config_file_path = @options[:proxy_config_file_path] || 'proxy_config'
        template_file_path = File.expand_path('./proxy_config.erb', File.dirname(__FILE__))
        template_content = File.open(template_file_path, 'r').readlines.join
        template = ERB.new(template_content, nil, '-')
        File.open(proxy_config_file_path, 'w') do |f|
          f.write(template.result(binding))
        end
      end

    end

  end

end
