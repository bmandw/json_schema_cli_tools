require "optparse"
require "json_schema_cli_tools/command/proxy_command"
require "json_schema_cli_tools/command/init_command"
require "json_schema_cli_tools/command/check_request_command"
require "json_schema_cli_tools/command/check_response_command"

module JsonSchemaCliTools

  module Command

    module Options

      module_function

      def parse!(argv)
        command_parser = OptionParser.new do |opts|
          opts.on("-v", "--version", "show version") do |p|
            print "#{JsonSchemaCliTools::VERSION}\n"
            exit
          end
        end
        command_parser.order!(argv)

        sub_command_name = argv.shift
        sub_command = sub_commands[sub_command_name]
        options = sub_command.parse!(argv)
        [sub_command, options]
      end

      def sub_commands
        sub_commands = Hash.new do |k, v|
          raise ArgumentError, "#{v} is not jsct sub command."
        end
        sub_commands['init'] = InitCommand
        sub_commands['proxy'] = ProxyCommand
        sub_commands['check-request'] = CheckRequestCommand
        sub_commands['check-response'] = CheckResponseCommand
        sub_commands
      end

    end

  end

end
