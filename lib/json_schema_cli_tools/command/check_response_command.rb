require "json"
require "json_schema"
require "json_schema_cli_tools/validator"
require "json_schema_cli_tools/command/mixin"

module JsonSchemaCliTools

  module Command

    class CheckResponseCommand
      include Mixin

      def self.print_usage!
        $stderr.puts "Usage: jsct check-response <schema_file_path> <schema_category> <api_title> <data_file_path>"
        $stderr.puts "       jsct check-response <schema_file_path> <schema_category> <api_title>"
      end

      def self.default_options
        {
          json_schema_file_path: "schema.json",
          schema_category: nil,
          api_title: nil,
          data_file_path: nil
        }
      end

      def self.parse!(argv)
        options = default_options
        OptionParser.new { |opts|
          opts.on("-h", "--help",) do |s|
            print_usage!
            exit
          end
        }.parse!(argv)

        options[:json_schema_file_path] = argv.shift
        options[:schema_category] = argv.shift
        options[:api_title] = argv.shift
        options[:data_file_path] = argv.shift
        options
      end

      def initialize(options)
        @options = options
      end

      def execute
        return false if !(schema_file_path = @options[:json_schema_file_path])
        return false if !(schema_category = @options[:schema_category])
        return false if !(api_title = @options[:api_title])
        return false if !(schema = parse_json_schema(schema_file_path))
        data_file_path = @options[:data_file_path]

        data = data_file_path.nil? ? read_json_stdin : read_json_file(data_file_path)
        validator = Validator.find_link_validator(schema, schema_category, api_title)
        validator.validate_response_body(data)
      end

    end

  end

end
