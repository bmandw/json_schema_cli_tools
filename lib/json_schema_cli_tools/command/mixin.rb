require "json"
require "json_schema"

module JsonSchemaCliTools

  module Command

    module Mixin

      def read_json_file(file)
        contents = File.read(file)
        return JSON.load(contents)
      end

      def parse_json_schema(file)
        if !(schema_data = read_json_file(file))
          return nil
        end
        schema  = JsonSchema.parse!(schema_data)
        schema.expand_references!
        return schema
      end

      def read_json_stdin()
        contents = $stdin.readlines.join.strip
        return JSON.load(contents)
      end

    end

  end

end
