require "optparse"
require "rack"
require "rack-json_schema"
require "logger"
require "json_schema_cli_tools"
require "json_schema_cli_tools/validation_server"
require "json_schema_cli_tools/response"
require "json_schema_cli_tools/command/mixin"

module JsonSchemaCliTools

  module Command

    class ProxyCommand
      include Mixin

      def self.print_usage!
        $stderr.puts "Usage: jsct proxy <schema_file_path> <proxy_config_file_path>"
        $stderr.puts "       jsct proxy <schema_file_path> <proxy_config_file_path> -d"
        $stderr.puts "       jsct proxy <schema_file_path> <proxy_config_file_path> -s <static_dir_path>"
        $stderr.puts "       jsct proxy <schema_file_path> <proxy_config_file_path> -p <port>"
      end

      def self.default_options
        {
          port: 8888,
          static_dir_path: ".",
          dump_traffic: false,
          json_schema_file_path: "schema.json",
          proxy_config_file_path: "proxy_config"
        }
      end

      def self.parse!(argv)
        options = default_options
        OptionParser.new { |opts|
          opts.on("-h", "--help",) do |s|
            print_usage!
            exit
          end
          opts.on("-p VALUE", "--port VALUE", "server port") do |p|
            options[:port] = p.to_i
          end
          opts.on("-s VALUE", "--static_dir VALUE", "static dir path") do |s|
            options[:static_dir_path] = s
          end
          opts.on("-d", "--dump_traffic",) do |s|
            options[:dump_traffic] = true
          end
        }.parse!(argv)

        options[:json_schema_file_path] = argv.shift
        options[:proxy_config_file_path] = argv.shift
        options
      end

      def initialize(options)
        @options = options
      end

      def execute
        schema = read_json_file(@options[:json_schema_file_path])
        static_dir_path = @options[:static_dir_path]
        dump_traffic = @options[:dump_traffic]
        port = @options[:port]
        proxy_config_file_path = @options[:proxy_config_file_path]

        logger = Logger.new(STDOUT)
        logger.formatter = proc do |severity, datetime, progname, msg|
          time = datetime.strftime("%Y-%m-%d %H:%M:%S")
          "[#{time}] #{severity} #{msg}\n"
        end

        proxy_config = JsonSchemaCliTools::ProxyConfig.load(proxy_config_file_path)
        response_generator = JsonSchemaCliTools::Response.new(proxy_config)

        app = Rack::Builder.new do
          use JsonSchemaCliTools::ValidationServer, schema: schema, logger: logger, response_generator: response_generator, dump_traffic: dump_traffic
          use Rack::Static, :urls => ["/"],  :root => static_dir_path, :index => "index.html"
          run ->(env) { [404, {}, ["Not found"]] }
        end
        # アクセスログは表示しない
        Rack::Server.new(:app => app, :Port => port, :AccessLog => []).start
      end

    end

  end

end
