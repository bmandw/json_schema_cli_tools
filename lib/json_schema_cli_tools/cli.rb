require "json_schema_cli_tools/command/options"

module JsonSchemaCliTools

  class Cli

    def self.run(argv)
      new(argv).execute
    end

    def initialize(argv)
      @argv = argv
    end

    def execute
      sub_command, options = Command::Options.parse!(@argv)
      unless sub_command.nil?
        begin
          sub_command.new(options).execute
        rescue ValidateError, InstancesRelTypeError => e
          $stderr.puts e.memo
          $stderr.puts e
        rescue => e
          $stderr.puts e
          exit(1)
        end
      end
    end

  end

end
