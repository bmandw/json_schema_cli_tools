module JsonSchemaCliTools

  class ValidateError < RuntimeError

    attr_accessor :memo, :errors

    def initialize(errors, memo='[ERROR]')
      @errors = errors
      @memo = memo
    end

    def to_s
      @errors.join("\n")
    end

  end

  class InstancesRelTypeError < RuntimeError

    attr_accessor :memo, :errors

    def initialize(link, memo='[ERROR]')
      fragment = "#/"+ link.parent.fragment + "/" + link.target_schema.fragment
      @errors = [
        "#: failed schema #{fragment}: `rel:instances`, but targetSchema type is not `array`. Recommend `rel:self`."
      ]
      @memo = memo
    end

    def to_s
      @errors.join("\n")
    end

  end

end
