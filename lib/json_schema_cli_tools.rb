require "json_schema_cli_tools/version"
require "json_schema_cli_tools/proxy_config"
require "json_schema_cli_tools/cli"
require "json_schema_cli_tools/response"

module JsonSchemaCliTools

  def self.proxy_configure(&block)
    ProxyConfig.define(&block)
  end

end
