# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'json_schema_cli_tools/version'

Gem::Specification.new do |spec|
  spec.name          = "json_schema_cli_tools"
  spec.version       = JsonSchemaCliTools::VERSION
  spec.authors       = ["Chihiro Ushiki"]
  spec.email         = ["c.ushiki@bmandw.com"]

  spec.summary       = %q{json schema tools}
  spec.description   = %q{json schema tools for web system development.}
  spec.homepage      = "https://bitbucket.org/bmandw/json_schema_tool.git"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "rack-test", "~> 0.6"
  spec.add_development_dependency "ruby-debug-ide", "~> 0.6.0"
  spec.add_development_dependency "debase", "~> 0.2.1"
  spec.add_dependency "json_schema", "~> 0.13"
  spec.add_dependency "rack", "~> 1.6"
  spec.add_dependency "rack-json_schema", "~> 1.5"
  spec.add_dependency "rack-reverse-proxy", "~> 0.11"
end
