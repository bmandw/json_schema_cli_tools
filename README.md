# Json Schema Cli Tools

JSON Schemaを利用したWEB API関連の開発支援コマンドラインツール。

## 利用可能なコマンド

* jsct check-request: HTTPリクエスト（クエリストリング or リクエストボディ）のバリデーション
* jsct check-response: HTTPレスポンスボディのバリデーション
* jsct proxy: HTTPリクエストとレスポンスのバリデーションリバースプロキシサーバ
* jsct init: プロキシ設定ファイルの雛形を生成する

### jsct check-request

#### HTTPリクエストボディJSONデータのバリデーションを行う

**使い方**
```
jsct check-request <JSON Schemaファイルのパス> <スキーマのカテゴリ> <対象となるlinksのタイトル> <JSONデータファイルパス>
jsct check-request <JSON Schemaファイルのパス> <スキーマのカテゴリ> <対象となるlinksのタイトル>
```

JSONデータファイルパスを省略した場合、標準入力でJSONデータを読み込む。

**使用例**

次のようなJSONスキーマ([fixture/schema.json](fixture/schema.json))のAPI（一部抜粋）
```json
        {
          "description": "Create a new user.",
          "href": "/users",
          "method": "POST",
          "rel": "create",
          "schema": {
            "properties": {
              "name": {
                "$ref": "#/definitions/user/definitions/name"
              }
            },
            "type": [
              "object"
            ],
            "required": [
              "name"
            ]
          },
          "title": "Create"
        },
```
で、HTTPリクエストに当たるJSONデータ([fixture/requests/user/Create.json](fixture/requests/user/Create.json))
```json
{
  "name": 12345
}
```
を検証する場合、次のようにコマンド実行する。

```
$ check-request fixture/schema.json user Create fixture/requests/user/Create.json
```

**バリデーションエラーの表示例**

JSONデータがスキーマ定義を満たさない場合、次のようにエラー表示される。

```
$ jsct check-request fixture/schema.json user Create fixture/requests/user/Create.json
[ERROR]
#/name: failed schema #/definitions/user/links/0/schema/properties/name: For 'properties/name', 12345 is not a string.
```

#### HTTPリクエストクエリストリングのバリデーションを行う

**使い方**
```
jsct check-request <JSON Schemaファイルのパス> <スキーマのカテゴリ> <対象となるlinksのタイトル> -q <クエリストリング>
```

**使用例**

次のようなJSONスキーマ([fixture/schema.json](fixture/schema.json))のAPI（一部抜粋）
```json
        {
          "description": "List existing users.",
          "href": "/users",
          "method": "GET",
          "schema": {
            "properties": {
              "date_from": {
                "description": "when user was created",
                "format": "date",
                "example": "2016-07-01",
                "type": [
                  "string"
                ]
              }
            },
            "type": [
              "object"
            ]
          },
          "rel": "instances",
          "title": "List"
        },
```
で、HTTPリクエストのクエリストリングを検証する場合、次のようにコマンド実行する。

```
$ jsct check-request fixture/schema.json user List -q "?date_from=2016-01-01"
```

**バリデーションエラーの表示例**

JSONデータがスキーマ定義を満たさない場合、次のようにエラー表示される。

```
$ jsct check-request fixture/schema.json user List -q "?date_from=abcd"
[ERROR:QUERY_STRING]
#/date_from: failed schema #/definitions/user/links/3/schema/properties/date_from: abcd is not a valid date.
```

### jsct check-response

HTTPレスポンスボディJSONデータのバリデーションを行う。

**使い方**
```
jsct check-response <JSON Schemaファイルのパス> <スキーマのカテゴリ> <対象となるlinksのタイトル> <JSONデータファイルパス>
jsct check-response <JSON Schemaファイルのパス> <スキーマのカテゴリ> <対象となるlinksのタイトル>
```

JSONデータファイルパスを省略した場合、標準入力でJSONデータを読み込む。

**使用例**

次のようなJSONスキーマ([fixture/schema.json](fixture/schema.json))のAPI（一部抜粋）
```json
        {
          "description": "Info for existing user.",
          "href": "/users/{(%23%2Fdefinitions%2Fuser%2Fdefinitions%2Fid)}",
          "method": "GET",
          "rel": "self",
          "title": "Info"
        },
```
で、HTTPレスポンスに当たるJSONデータ([fixture/responses/user/Info.json](fixture/responses/user/Info.json))
```json
{
  "name": 12345
}
```
を検証する場合、次のようにコマンド実行する。

```
$ jsct check-response fixture/schema.json user Info fixture/responses/user/Info.json
```

**バリデーションエラーの表示例**

JSONデータがスキーマ定義を満たさない場合、次のようにエラー表示される。

```
$ jsct check-response fixture/schema.json user Info fixture/responses/user/Info.json
[ERROR]
#/name: failed schema #/definitions/user/properties/name: For 'properties/name', 12345 is not a string.
#: failed schema #/definitions/user: "id" wasn't supplied.
```

### jstc proxy

HTTPリクエストとHTTPレスポンスデータのバリデーションを行うプロキシサーバを起動する。

-d(--dump_traffic)を指定すると、プロキシされたHTTPリクエストボディ・レスポンスボディをログに表示する。

**使い方**

```
jstc proxy <JSON Schemaファイルのパス> <プロキシ設定ファイルへのパス>
jstc proxy <JSON Schemaファイルのパス> <プロキシ設定ファイルへのパス> -d
jstc proxy <JSON Schemaファイルのパス> <プロキシ設定ファイルへのパス> -s <スタティックファイルなどのディレクトリパス>
jstc proxy <JSON Schemaファイルのパス> <プロキシ設定ファイルへのパス> -p <プロキシサーバのポート番号>
```

**使用例**

JSONスキーマ([fixture/schema.json](fixture/schema.json))とプロキシ設定ファイル([fixture/proxy_config](fixture/proxy_config))を用意し、次のようにコマンド実行する。

```
$ jstc proxy fixture/schema.json fixture/proxy_config
```

**プロキシ設定**

リクエストメソッドとパスごとに、静的なダミーファイルかAPIサーバへプロキシしてレスポンスするかを設定する。

設定例を下に示す。

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

JsonSchemaCliTools.proxy_configure do
  # GET /users/123 のようなリクエストは静的ファイルでレスポンスを返す
  get '/users/(\d+)', static: 'responses/user/get-users.json'
  # DELETE /users/123 のようなリクエストはステータスコード204で返し、レスポンスボディはなしとする
  delete '/users/(\d+)', status: 204
  # POST /groups のリクエストは静的ファイルでレスポンスを返し、ステータスコードは201とする
  post '/groups', static: 'responses/group/post-group.json', status: 201
  # GET /groups/123 のようなリクエストはERBテンプレートでレスポンスを返す
  get '/groups/(\d+)', erb: 'responses/group/get-group.erb'
  # PUT /groups/123 のようなリクエストはすデータスコード202で、ERBテンプレートでレスポンスを返す
  put '/groups/(\d+)', erb: 'responses/group/put-group.erb', status: 202
  # リクエストメソッドを問わずに、 /users へのリクエストを実際のAPIサーバへプロキシする
  any '/users', proxy: 'http://localhost:8080/users'
end
```

**ERBテンプレートでのダミーレスポンス**

jsct proxyではERBテンプレートを用いて、ダミーレスポンスを返すことができる。ERBテンプレートの例を下に示す。

```ruby
{
  "id": <%= args[0] %>,
  "name": "<%= body["name"] %>"
}
```

proxy設定でリクエストパスの正規表現にマッチした値はargsリストでテンプレートから利用できる。また、リクエストボディがある場合、bodyハッシュオブジェクトからキーを指定して参照できる。


**バリデーションエラーの表示例**

HTTPリクエストとダミーレスポンスデータそれぞれを検証し、スキーマ定義を満たさない場合、次のようにエラー表示される。

```
$ jsct proxy schema.json proxy_config
[2016-07-25 17:43:07] INFO  WEBrick 1.3.1
[2016-07-25 17:43:07] INFO  ruby 2.3.1 (2016-04-26) [x86_64-linux]
[2016-07-25 17:43:07] INFO  WEBrick::HTTPServer#start: pid=26083 port=8888
[2016-07-25 17:44:09] INFO [Handle Response API Server] "POST /users"
[2016-07-25 17:44:09] ERROR [JSON Schema Invalid HTTP Request Error] POST /users =====>
[2016-07-25 17:44:09] ERROR   Request Body: {"name":12345}
[2016-07-25 17:44:09] ERROR   Reason:
[2016-07-25 17:44:09] ERROR     #/name: failed schema #/definitions/user/links/0/schema/properties/name: For 'properties/name', 12345 is not a string.
[2016-07-25 17:44:09] ERROR <=====
[2016-07-25 17:44:10] ERROR [JSON Schema Invalid HTTP Response Error] POST /users =====>
[2016-07-25 17:44:10] ERROR   API Server: http://localhost:3000
[2016-07-25 17:44:10] ERROR   Response Body: {"name":"123"}
[2016-07-25 17:44:10] ERROR   Reason:
[2016-07-25 17:44:10] ERROR     #: failed schema #/definitions/user: "created_at", "id", "updated_at" weren't supplied.
[2016-07-25 17:44:10] ERROR <=====
```

**スキーマ定義にないURLへのリクエスト**

該当するローカルの静的ファイルがレスポンスとして返される。このような場合、ログには次のように表示される。

```
[2016-08-23 17:45:13] INFO [Handle Local Static File] "GET /group.json"
```

### jstc init

jstc proxy コマンドで使用するプロキシ設定の雛形を生成する。

**使い方**

```
jstc init <JSON Schemaファイルのパス> <出力するプロキシ設定ファイルのパス>
```


## インストール

### Gemファイルからシステムへインストールする場合

```
$ gem install json_schema_cli_tools-x.x.x.gem
```

### Bundlerを使ってリポジトリからインストールする場合

Gemfileにbitbucketのリポジトリから取得できるように定義
```
git_source(:bitbucket) do |repo_name|
    "git@bitbucket.org:#{repo_name}.git"
end
gem 'json_schema_cli_tools', :bitbucket => 'bmandw/json_schema_cli_tools'
```
Bundlerでインストール
```
$ bundle install --path .bundle
$ bundle exec check-request
```

## 補足事項

### 依存関係

redcarpetのインストールにlibgmp-devが必要になる。ubuntuの場合はaptでインストールする。

```
$ sudo apt-get install libgmp-dev
```

### gemファイルの生成

```
$ gem build json_schema_cli_tools.gemspec
```

### Windowsでの使い方

Windowsで使用する場合、RubyのインストールとDevKitのインストールを行い、json_schema_cli_toolsをgemファイルからインストールして利用する。

動作確認済みの環境は次の通り。

* Windows 7 Professional SP1 32bit
* Ruby 2.3.1

#### RubyとDevKitのインストール

RubyInstallerでインストールする。

[ダウンロードページ](http://rubyinstaller.org/downloads/)より、
2.3.1 [32bit](http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.3.1.exe)または[64bit](http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.3.1-x64.exe)をダウンロード。

インストーラで「Rubyの実行ファイルへ環境変数PATHを設定する」にチェックして、インストールを実行する。

コマンドプロンプトで「ruby -version」を実行し、バージョンが表示されることを確認する。

DevKit[32bit](http://dl.bintray.com/oneclick/rubyinstaller/DevKit-mingw64-32-4.7.2-20130224-1151-sfx.exe)または[64bit](http://dl.bintray.com/oneclick/rubyinstaller/DevKit-mingw64-64-4.7.2-20130224-1432-sfx.exe)をダウンロードし、
例として C:\RubyDevKit に展開する。

展開後、コマンドプロンプトで次のように実行してインストールする。

```
> cd C:\RubyDevKit
> ruby dk.rb init
> ruby dk.rb install
[INFO] Updating convenience notice gem override for 'C:/Ruby23'
[INFO] Installing 'C:/Ruby23/lib/ruby/site_ruby/devkit.rb'
```
上のようなINFOメッセージが表示されたらインストール完了。

#### json_schema_cli_toolsのインストール

コマンドプロンプトを開き、次のコマンドを実行する。

```
> gem install json_schema_cli_tools-x.x.x.gem
```
installedが表示されたら完了。

以上で、コマンドプロンプトでjson_schema_cli_toolsのコマンドが使えるようになる。
